import { Component, OnInit } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem, copyArrayItem } from '@angular/cdk/drag-drop';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-draggable',
  templateUrl: './draggable.component.html',
  styleUrls: ['./draggable.component.scss'],
})
export class DraggableComponent implements OnInit {
  result: number;
  X: number;

  todo = [];
  done = [];

  constructor(private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.result = 0;
    this.X = 0;

    this.todo = [
      '&#8730;/X',
      '1/X',
      'e^X',
    ];
  }

  calc() {
    this.result = this.X;

    if (this.result === 0) {
      this._snackBar.open('You can\'t use 0 as value', 'Close', {
        duration: 3 * 1000,
      });
      return;
    }

    for (const e of this.done) {
      switch (e) {
        case '&#8730;/X':
          console.log('&#8730;/X');
          this.result = Math.sqrt(this.result);
          break;
        case '1/X':
          console.log('1/X');
          this.result = 1 / this.result;
          break;
        case 'e^X':
          console.log('e^X');
          this.result = Math.pow(Math.E, this.result);
          break;
      }
    }
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      if (event.container.element.nativeElement.classList.contains('equation')) {
        const arr = [];
        transferArrayItem(event.previousContainer.data,
          arr,
          event.previousIndex,
          event.currentIndex);
        this.done.splice(event.currentIndex, 1);
      } else {
        copyArrayItem(event.previousContainer.data,
          event.container.data,
          event.previousIndex,
          event.currentIndex);
      }
    }

    this.calc();
  }

}
