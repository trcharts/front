import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { ChartModule } from '../chart/chart.module';
import { SharedModule } from '../shared/shared.module';
import { DashboardRoutes } from './dashboard.routing';
import { DraggableComponent } from './draggable/draggable.component';



@NgModule({
  declarations: [DashboardComponent, DraggableComponent],
  imports: [
    CommonModule,
    SharedModule,
    ChartModule,
    DashboardRoutes,
  ]
})
export class DashboardModule { }
