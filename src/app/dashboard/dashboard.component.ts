import { Component, Inject, OnInit, PLATFORM_ID } from '@angular/core';
import { BarData, UTCTimestamp } from 'lightweight-charts';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { isPlatformServer } from '@angular/common';
import * as moment from 'moment';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  chartData: BarData[] = [];
  // tmp data for select
  symbols: [
    { label: 'BTCUSDT', value: 'BTCUSDT' },
    { label: 'LTCUSDT', value: 'LTCUSDT' },
    { label: 'TRXUSDT', value: 'TRXUSDT' },
    { label: 'TRXBTC', value: 'TRXBTC' }
  ];
  intervals: ['1m', '3m', '5m', '15m', '1h', '4h', '1d'];

  symbolForm: FormGroup; // form to change klines

  constructor(
    @Inject(PLATFORM_ID) private platformId: object,
    private _http: HttpClient,
    private _fb: FormBuilder,
  ) {
    this.symbols = [
      { label: 'BTCUSDT', value: 'BTCUSDT' },
      { label: 'LTCUSDT', value: 'LTCUSDT' },
      { label: 'TRXUSDT', value: 'TRXUSDT' },
      { label: 'TRXBTC', value: 'TRXBTC' },
    ];

    this.intervals = ['1m', '3m', '5m', '15m', '1h', '4h', '1d'];
  }

  ngOnInit(): void {
    if (isPlatformServer(this.platformId)) {
      return;
    }

    this.symbolForm = this._fb.group({
      symbol: new FormControl('BTCUSDT'),
      interval: new FormControl('15m'),
      start_time: new FormControl('2020-04-24'),
      end_time: new FormControl('2020-04-29'),
    });
  }

  getKlines(): void {
    // transform data from form to queryParams
    const params = {
      symbol: this.symbolForm.value.symbol,
      interval: this.symbolForm.value.interval,
      start_time: moment(this.symbolForm.value.start_time).utc(false).format(),
      end_time: moment(this.symbolForm.value.end_time).utc(false).format(),
    };

    this._http.get(`${environment.httpApi}/klines`, { params }).subscribe((res: any) => {
      // fill klines to chart
      this.chartData = new Array(res.length);
      for (let i = 0; i < res.length; i++) {
        this.chartData[i] = {
          time: moment(res[i].close_time, 'YYYY-MM-DDTHH:mm:ss').unix() as UTCTimestamp,
          open: res[i].open,
          high: res[i].high,
          low: res[i].low,
          close: res[i].close,
        };
      }
    });
  }

  socketTest() {
    const ws = new WebSocket(`${environment.wsApi}/ws/ticker/BIN/interval/1m/symbol/BTCUSDT`);

    let i = 0;

    ws.onmessage = (event) => {
      const data = JSON.parse(event.data);
      console.log(data);
      if (++i === 6) {
        ws.close();
      }
    };
  }

  ping() {
    this._http.get(`${environment.httpApi}/ping`).subscribe(res => {
      console.log(res);
    });
  }

}
