import { Component, ElementRef, Inject, Input, OnChanges, OnInit, PLATFORM_ID, SimpleChanges, ViewChild } from '@angular/core';
import { BarData, createChart, IChartApi, ISeriesApi } from 'lightweight-charts';
import { DOCUMENT, isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss'],
})
export class ChartComponent implements OnInit, OnChanges {
  @ViewChild('chart', { static: true }) child!: ElementRef; // обращение к элементу старницы
  private chart: IChartApi; // чарт
  private candlestickSeries: ISeriesApi<'Candlestick'>; // даныне для чарта (приходят из chartData)
  @Input() public chartData: BarData[] = []; // входные данные для чарта

  constructor(
    @Inject(DOCUMENT) private document: Document, // инициализация библиотек
    // tslint:disable-next-line:ban-types
    @Inject(PLATFORM_ID) private _platformId: Object, // инициализация библиотек
  ) { }

  ngOnChanges(changes: SimpleChanges): void {
    this.candlestickSeries?.setData(this.chartData); // обновление данных на чарте
  }

  ngOnInit(): void {
    // chart initialization

    if (isPlatformBrowser(this._platformId)) { // проверка в браузере или ан сервере
      this.chart = createChart(this.child.nativeElement, { // инициализация чарта
        width: 600, // шиирна
        height: 450, // высота
        priceScale: { autoScale: true }, // масштабирвоание
      });
    }
    this.candlestickSeries = this.chart.addCandlestickSeries(); // инициализация массива

    this.candlestickSeries.setData(this.chartData); // инициализация чарта
  }
}
