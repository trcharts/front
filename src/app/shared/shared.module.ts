import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCardModule } from '@angular/material/card';
import { MatNativeDateModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatButtonModule } from '@angular/material/button';
import { HttpClientModule } from '@angular/common/http';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatSnackBarModule } from '@angular/material/snack-bar';


const MATERIAL_MODULES = [

];

@NgModule({
  declarations: [],
  imports: [
      MatSnackBarModule,
  MatInputModule,
  MatFormFieldModule,
  DragDropModule,
  MatCardModule,
  MatNativeDateModule,
  MatSelectModule,
  MatDatepickerModule,
  MatButtonModule,

    CommonModule,

    ReactiveFormsModule,
    HttpClientModule,
  ],
  exports: [
      MatSnackBarModule,
  MatInputModule,
  MatFormFieldModule,
  DragDropModule,
  MatCardModule,
  MatNativeDateModule,
  MatSelectModule,
  MatDatepickerModule,
  MatButtonModule,

    ReactiveFormsModule,
    HttpClientModule,
  ]
})
export class SharedModule { }
