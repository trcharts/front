export const environment = {
  production: true,
  httpApi: 'http://localhost/api',
  wsApi: 'ws://localhost/api',
};
